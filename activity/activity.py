from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age
    
    def get_name(self):
        return self.name
    
    def get_breed(self):
        return self.breed
    
    def get_age(self):
        return self.age
    
    def set_name(self, name):
        self.name = name
        
    def set_breed(self, breed):
        self.breed = breed
        
    def set_age(self, age):
        self.age = age
        
    def eat(self, food):
        print(f"{self.name} is eating {food}, Woof")
        
    def make_sound(self):
        print(f"{self.name} Meow!")
        
    def call(self):
        print(f"{self.name}, come here doggy")
        
class Dog(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age
    
    def get_name(self):
        return self.name
    
    def get_breed(self):
        return self.breed
    
    def get_age(self):
        return self.age
    
    def set_name(self, name):
        self.name = name
        
    def set_breed(self, breed):
        self.breed = breed
        
    def set_age(self, age):
        self.age = age
        
    def eat(self, food):
        print(f"{self.name} is eating {food}, Nya")
        
    def make_sound(self):
        print(f"{self.name} Woofs!")
        
    def call(self):
        print(f"{self.name} come here kitty")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Perian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()